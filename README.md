
RTP: Reliable Transport Protocol
=========

The programming assignment of interest is “Implementing a Reliable Transport Protocol” (programming assignment 5) from Pearson student resources for the Kurose/Ross book.

## About
This is a makeable project of the file, where the the student only need to implement rtp_abp.c (part a) and/or rtp_gbn.c (part b/extra credit).

## What?
This is just a cleared up code, to make life easier.
